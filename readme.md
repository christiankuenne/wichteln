# Wichteln

Kleines Tool, um Wichtel-Mails zu verschicken.

## Installation

Dependencies installieren:

`dotnet restore`

## Konfiguration

Passe die [Teilnehmerliste](config/teilnehmer.csv), den [E-Mail-Text](config/email.txt) an und die [E-Mail-Servereinstellungen](config/email.ini) an.

## Benutzung

Starte das Programm:

`dotnet run`
