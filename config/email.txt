Hallo {0},

die Karten wurden neu gemischt. Und das ist eine gute Nachricht. Vor allem für {1}. Denn {1} bekommt dieses Jahr ein Wichtelgeschenk. Und zwar von dir.

Worüber würde sich {1} wohl freuen? Ich glaube, {1} würde sich bestimmt über Socken freuen. Aber vielleicht fällt dir ja etwas besseres ein.


Viele Grüße


Der Weihnachtsmann
