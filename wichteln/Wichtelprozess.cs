﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Wichteln
{
    public class Wichtelprozess
    {
        private List<Person> personen;

        private void LoadPersonen() {
            personen = new List<Person>();
            IEnumerable<string> lines = File.ReadLines("config/teilnehmer.csv");
            foreach (string str in lines) {
                string[] pair = str.Split(',');
                personen.Add(new Person(pair[0].Trim(), pair[1].Trim()));
            }
        }

        public void Wichteln() {
            // Personen laden und mischen
            LoadPersonen();
            personen.Sort();
            // E-Mail Text laden
            string template = File.ReadAllText("config/email.txt");
            for (int i = 0; i < personen.Count; ++i) {
                int j = (i + 1) % personen.Count;
                string text = String.Format(template, personen[i].Name, personen[j].Name);
                var mail = new Mail(personen[i].Name, personen[i].Email, "Wichteln", text);
                mail.Send();
                Console.WriteLine(String.Format("Mail {0} gesendet.", i + 1));
            }
        }
    }
}
