﻿using System;

namespace Wichteln
{
    public class Person : IComparable
    {
        private static Random random = new Random();
        public string Name { get; set; }
        public string Email { get; set; }
        private int orderToken;

        public Person(string name, string email) {
            this.Name = name;
            this.Email = email;
            this.orderToken = random.Next();
        }

        public int CompareTo(Object obj) {
            if (obj == null || obj.GetType() != this.GetType()) {
                return -1;
            }
            Person other = (Person) obj;
            if (this.orderToken > other.orderToken) {
                return -1;
            } else if (this.orderToken < other.orderToken) {
                return 1;
            } else {
                return this.Email.CompareTo(other.Email);
            }
        }
    }
}
