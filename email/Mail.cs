using MailKit.Net.Smtp;
using MimeKit;
using System.Collections.Generic;
using System.IO;

namespace Wichteln
{
    public class Mail
    {
        private static IDictionary<string, string> config;
        private string toName;
        private string toAddress;
        private string subject;
        private string body;       

        public Mail(string toName, string toAddress, string subject, string body) {
            this.toName = toName;
            this.toAddress = toAddress;
            this.subject = subject;
            this.body = body;
            LoadConfig();
        }

        private void LoadConfig() {
            if (config == null) {
                config = new Dictionary<string, string>();
                string[] lines = File.ReadAllLines("config/email.ini");
                foreach (string line in lines) {
                    string[] pair = line.Split('=');
                    config.Add(pair[0].Trim(), pair[1].Trim());
                }
            }
        }

        public void Send() {
            var message = new MimeMessage ();
            message.From.Add (new MailboxAddress (config["name"], config["address"]));
            message.To.Add (new MailboxAddress (toName, toAddress));
            message.Subject = subject;
            message.Body = new TextPart ("plain") {
                Text = body
            };
            using (var client = new SmtpClient ()) {
                // For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
                client.ServerCertificateValidationCallback = (s,c,h,e) => true;
                client.Connect (config["server"], int.Parse(config["port"]), false);
                // Note: since we don't have an OAuth2 token, disable
                // the XOAUTH2 authentication mechanism.
                client.AuthenticationMechanisms.Remove ("XOAUTH2");
                // Note: only needed if the SMTP server requires authentication
                client.Authenticate (config["address"], config["password"]);
                client.Send (message);
                client.Disconnect (true);
            }
        }
    }
}